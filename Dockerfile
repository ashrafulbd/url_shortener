FROM python:3

ENV PYTHONUNBEFFERED 1

RUN mkdir Projects

WORKDIR /Projects

COPY . ./Projects/


RUN cd Projects/ && pip install -r requirements.txt
