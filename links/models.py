import random, string
from datetime import date
import datetime

from django.db import models

import operator as op
from functools import reduce


from django.core.mail import send_mail


from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


def ncr(n, r):

    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom


def check_r():
    total_links = SingleLink.objects.count()
    # print(total_links)

    for i in range(2, 512):
        k = ncr(62, i)

        if k > total_links:
            break
    return i+1






def random_string():

    range_len = check_r()
    ran = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(range_len))

    return ran



class SingleLink(models.Model):

    url_char = models.URLField(blank=False, null=False, validators=[URLValidator])
    short_url_char = models.CharField(unique=True, default=random_string, max_length=10, blank=False, null=False)
    created_at = models.DateTimeField(auto_now=True)
    email = models.EmailField(max_length=70, blank=True, null=True)

    def save(self, *args, **kwargs):


        today = date.today()
        end_date = today + datetime.timedelta(days=30)
        end_date_str = end_date.strftime('%d-%m-%Y')
        to_email = []

        data = SingleLink.objects.filter(url_char=self.url_char).count()

        if data > 0:

            # self.short_url_char = data.short_url_char
            # self.save()

            if self.email:


                to_email.append(self.email)
                send_mail(
                    'Your Short Link is active for 30 more days!',
                    'Your requested link for:  ' + self.url_char + ',your desired shortened link is:  '
                        + self.short_url_char + '.' + 'Your short link will be available for next 30 days, till '
                        + end_date_str
                        ,
                    'shorturlbd@gmail.com',
                    to_email,
                    fail_silently=False,
                )

            # print("again")
            # self.short_url_char = data.short_url_char
            obj = SingleLink.objects.filter(url_char=self.url_char).update(created_at=datetime.datetime.now(tz=datetime.timezone.utc))
            print(obj)
            return obj
            # data.save()
            # super(SingleLink, self).save(*args, **kwargs)

        else:

            # if not self.url_char.startswith("http"):
            #     self.url_char = "http://" + self.url_char


            if self.email:

                to_email.append(self.email)
                send_mail(
                    'Successfully Generated the Short Link',
                    'Your requested link for:  ' + self.url_char + ',your desired shortened link is:  '
                        + self.short_url_char + '.' + 'Your shor link will be available for next 30 days, till '
                        + end_date_str
                        ,
                    'shorturlbd@gmail.com',
                    to_email,
                    fail_silently=False,
                )
            # print(self.url_char)
            super(SingleLink, self).save(*args, **kwargs)




    def __str__(self):
        return self.url_char + "  " + str(self.short_url_char) + "  " + str(self.created_at)

