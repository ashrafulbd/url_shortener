from django.urls import path, include


# from url_shortener.links import views
# from url_shortener.links import views
from django.views.generic import TemplateView

from links import views
# from links.views import reg_link_view

urlpatterns = [

    # path('', ),
    path('<str:s_url>/', views.redirect_page, name='redirect'),
    # path('', TemplateView.as_view(template_name='index.html'), name='home'),  # new
    path('', TemplateView.as_view(template_name='index.html'), name='index'),  # new
    # path('', reg_link_view),  # new
    path('55/55/', views.ajax_call, name='ajax_call'),




]
