from django import forms
from django.forms import ModelForm

from links.models import SingleLink


class RegLinkForm(forms.Form):

    url_char = forms.URLField(label="Url link", required=True,)  # always 11 digit 01700000000
    url_char.widget = forms.TextInput(attrs={'size': 45, 'placeholder': 'Enter your URL'})

    # short_url_char = forms.CharField(label="Business Name*")
    # short_url_char.widget = forms.TextInput(attrs={'size': 45, 'placeholder': 'Enter your business name',})

    email = forms.EmailField(label="Email", required=False)
    email.widget = forms.TextInput(attrs={'size': 45, 'placeholder': 'Enter your email'})
