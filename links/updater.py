from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
# from url_shortener.links.models import SingleLink
from links.models import SingleLink
import datetime
from django.utils import timezone




def clean_DB():
    all_links = SingleLink.objects.all()

    for x in all_links:

        created_date = x.created_at.replace(tzinfo=None)
        today = datetime.datetime.now()
        totaldays = (today - created_date)


        if totaldays.days > 29:
            x.delete()


        if totaldays.days == 25:

            if x.email:

                today_date = date.today()
                end_date = today_date + datetime.timedelta(days=5)
                end_date_str = end_date.strftime('%d-%m-%Y')


                to_email = []
                to_email.append(x.email)
                send_mail(
                    '5 Days left for your link',
                    'Your requested link for:  ' + self.url_char + ' and your desired shortened link is:  ' 
                        + self.short_url_char + '.' + 'Your shor link will be unavailable in five days at '
                        + end_date_str +'. If you want to keep the link alive, click here or try to regenarate the link again for more 30 days. If you do not re active the link in five days, you will not get the previous link back!'
                        ,
                    'shorturlbd@gmail.com',
                    to_email,
                    fail_silently=False,
                )




def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(clean_DB, trigger='cron', hour='20', minute='30')
    print("cleaning the Database!")
    scheduler.start()