# Generated by Django 2.2.3 on 2019-08-01 07:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0004_singlelink_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='singlelink',
            name='created_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
