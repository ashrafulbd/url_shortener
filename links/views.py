from django.shortcuts import render

from django import forms


# Create your views here.
# from django.core.exceptions import ObjectDoesNotExist

from django.shortcuts import get_object_or_404
from django.http import HttpResponse

from django.shortcuts import redirect

# from url_shortener.links.models import SingleLink
from links.forms import RegLinkForm
from links.models import SingleLink

from datetime import date
import datetime



def redirect_page(request, s_url):

    # try:
    #     link_instance = SingleLink.objects.get(short_url_char=s_url)
    # except ObjectDoesNotExist:
    #     link_instance = None
        # raise forms.ValidationError('Link not found, check your email')
    # link_instance = SingleLink.objects.get(short_url_char=s_url)
    link_instance = get_object_or_404(SingleLink, short_url_char=s_url)


    if link_instance:
        context = {
            'link_instance': link_instance
        }
        
        return render(request, "redirect.html", context)


#
# def reg_link_view(request):
#     # print(1)
#     # created_obj = None
#     saved_data = None
#     if request.method == 'GET':
#         form = RegLinkForm()
#         # created_obj = None
#         # print(created_obj)
#     # print(12)
#     elif request.method == 'POST':
#         form = RegLinkForm(request.POST)
#         if form.is_valid():
#             # print(form.cleaned_data)
#             # SingleLink.objects.create(**form.cleaned_data)
#             saved_data = "yeahhh"
#             SingleLink.objects.create(**form.cleaned_data)
#             # print(saved_data.short_url_char)
#             # form = RegLinkForm()
#         else:
#             print(form.errors)
#
#     # print(123)
#     context = {
#         'form': form,
#         'saved_data': saved_data
#     }
#     return render(request, "index.html", context)

    # return HttpResponse(saved_data)





def ajax_call(request):

    if request.method == 'POST':
        form = RegLinkForm(request.POST)
        if form.is_valid():
            # print(form.cleaned_data['url_char'])
            data = SingleLink.objects.filter(url_char=form.cleaned_data['url_char']).count()

            if data > 0:
                SingleLink.objects.filter(url_char=form.cleaned_data['url_char']).update(created_at=datetime.datetime.now(tz=datetime.timezone.utc))
                saved_data = SingleLink.objects.get(url_char=form.cleaned_data['url_char'])
                print(type(saved_data))
            else:
                saved_data = SingleLink.objects.create(**form.cleaned_data)
            return HttpResponse(saved_data)
        else:
            return HttpResponse("Error")




# def validate_url(request):
#     url_char = request.GET.get('url_char', None)
#     is_taken = SingleLink.objects.filter(url_char__iexact=url_char).exists()
#     data = {
#         'is_taken': is_taken
#     }
#     return JsonResponse(data)