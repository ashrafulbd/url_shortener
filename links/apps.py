from django.apps import AppConfig


class LinksConfig(AppConfig):
    name = 'links'


    def ready(self):
        from links import updater
        updater.start()